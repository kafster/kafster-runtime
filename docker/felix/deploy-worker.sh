#!/bin/sh
rm -rf felix-cache
cat ../deploy/bootstrap.txt \
    ../deploy/core.txt \
    ../deploy/http.txt \
    ../deploy/spring.txt \
    ../deploy/osgi.txt \
    ../deploy/pax-wrap.txt \
    ../deploy/k8s.txt \
    ../deploy/kafka.txt \
    ../deploy/worker.txt | \
    java -jar bin/felix.jar
