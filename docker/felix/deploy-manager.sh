#!/bin/sh
rm -rf felix-cache
cat ../deploy/bootstrap.txt \
    ../deploy/core.txt \
    ../deploy/http.txt \
    ../deploy/spring.txt \
    ../deploy/osgi.txt \
    ../deploy/pax-wrap.txt \
    ../deploy/graph.txt \
    ../deploy/k8s.txt \
    ../deploy/kafka.txt \
    ../deploy/manager.txt | \
    java -jar bin/felix.jar
