felix:start \
 mvn:com.google.code.findbugs/jsr305/3.0.2 \
 mvn:org.apache.servicemix.bundles/org.apache.servicemix.bundles.okio/1.15.0_1 \
 mvn:org.apache.servicemix.bundles/org.apache.servicemix.bundles.okhttp/3.12.1_1 \
 mvn:com.fasterxml.jackson.datatype/jackson-datatype-jsr310/2.13.1 \
 mvn:io.fabric8/kubernetes-model-common/5.12.1 \
 wrap:mvn:io.fabric8/kubernetes-model-core/5.12.1\$overwrite=merge\&Import-Package=\*\;resolution:\=optional \
 mvn:io.fabric8/kubernetes-model-admissionregistration/5.12.1 \
 mvn:io.fabric8/kubernetes-model-apiextensions/5.12.1 \
 mvn:io.fabric8/kubernetes-model-apps/5.12.1 \
 mvn:io.fabric8/kubernetes-model-autoscaling/5.12.1 \
 mvn:io.fabric8/kubernetes-model-batch/5.12.1 \
 mvn:io.fabric8/kubernetes-model-certificates/5.12.1 \
 mvn:io.fabric8/kubernetes-model-coordination/5.12.1 \
 mvn:io.fabric8/kubernetes-model-discovery/5.12.1 \
 mvn:io.fabric8/kubernetes-model-events/5.12.1 \
 mvn:io.fabric8/kubernetes-model-extensions/5.12.1 \
 mvn:io.fabric8/kubernetes-model-flowcontrol/5.12.1 \
 mvn:io.fabric8/kubernetes-model-metrics/5.12.1 \
 mvn:io.fabric8/kubernetes-model-networking/5.12.1 \
 mvn:io.fabric8/kubernetes-model-node/5.12.1 \
 mvn:io.fabric8/kubernetes-model-policy/5.12.1 \
 mvn:io.fabric8/kubernetes-model-rbac/5.12.1 \
 mvn:io.fabric8/kubernetes-model-scheduling/5.12.1 \
 mvn:io.fabric8/kubernetes-model-storageclass/5.12.1 \
 mvn:io.fabric8/zjsonpatch/0.3.0 \
 mvn:io.fabric8/kubernetes-client/5.12.1//bundle

